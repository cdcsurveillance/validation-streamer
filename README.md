# Validator-Streamer

This is a Kafka Streamer to validate participants. 

A producer must place a message on the participant's topic with PARTICIP as key and the content 
expected by the validator as the message. Each message will be processed by this streamer, which
will call the validator and save the error report on the outgoing topic.

### Running locally for testing.

In order to run this for testing, first start zookeeper and kafka.
You can use the provided docker-compose to start them.

Make sure you provide the following environment variables:
* KAFKA_BROKER
* APP_INCOMING_TOPIC
* APP_OUTGOIG_TOPIC
* VALIDATOR_URL


