package gov.cdc.nccd.esurveillance.validation.streamer

import com.fasterxml.jackson.databind.JsonNode
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client


@Client(value = "{url}",
        configuration = ValidatorClientConfig::class)
interface ValidatorClient {

    @Post("/validate/dprp/json")
    fun validate(@Body content: JsonNode): String
}