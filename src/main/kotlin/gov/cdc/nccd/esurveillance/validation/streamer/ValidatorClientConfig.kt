package gov.cdc.nccd.esurveillance.validation.streamer

import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.runtime.ApplicationConfiguration
import io.micronaut.context.annotation.ConfigurationProperties




@ConfigurationProperties("validator")
class ValidatorClientConfig(applicationConfiguration: ApplicationConfiguration?) : HttpClientConfiguration(applicationConfiguration) {

    lateinit var url: String

    var connectionPoolConfiguration: ValidatorClientConnectionPoolConfiguration? = null

    constructor(appConfig:ApplicationConfiguration, connectionPoolConfiguration:ValidatorClientConnectionPoolConfiguration): this(appConfig) {
        this.connectionPoolConfiguration = connectionPoolConfiguration
    }

    override fun getConnectionPoolConfiguration(): ConnectionPoolConfiguration {
        return connectionPoolConfiguration!!
    }

    @ConfigurationProperties("app")
    class ValidatorClientConnectionPoolConfiguration : ConnectionPoolConfiguration()
}