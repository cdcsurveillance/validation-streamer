package gov.cdc.nccd.esurveillance.validation.streamer

import io.micronaut.configuration.kafka.streams.ConfiguredStreamBuilder
import io.micronaut.context.annotation.Factory
import org.apache.kafka.streams.kstream.KStream
import javax.inject.Named
import javax.inject.Singleton

import com.fasterxml.jackson.databind.JsonNode
import io.micronaut.http.client.exceptions.HttpClientResponseException
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.connect.json.JsonSerializer
import org.apache.kafka.connect.json.JsonDeserializer
import org.apache.kafka.streams.KeyValue


import org.apache.kafka.streams.kstream.Consumed
import java.util.logging.Logger
import javax.inject.Inject

/**
 * This kafka Streamer listens to a topic of Participants.
 * The Key should be the participant ID, the value the content of it's rows.
 *
 * The streamer will pick a participant and validate its content.
 * The Error Report will be placed on the Outgoing Topic, also using the participant ID as key.
 *
 * @Created - 2019-05-20
 * @Author Marcelo Caldas mcq1@cdc.gov
 */
@Factory
class ValidatorStreamer(@Inject val appConfig: AppConfig,
                        @Inject val validator:ValidatorClient) {

    companion object {
        val LOG = Logger.getLogger(ValidatorStreamer::javaClass.name)
    }

//    @Inject
//    lateinit var validator: ValidatorClient

    @Singleton
    @Named("dprp-validator")
    fun validateParticipant(builder: ConfiguredStreamBuilder): KStream<String, JsonNode> {
        LOG.info("AUDIT - Validate Participant Streamer started")

        val jsonSerde = Serdes.serdeFrom<JsonNode>(JsonSerializer(), JsonDeserializer())
        val validationStreams: KStream<String, JsonNode> = builder.stream(appConfig.incomingtopic,Consumed.with(Serdes.String(), jsonSerde))

        val errorReports = validationStreams.map { k, v ->
            println("key: $k; Val: $v")
            val validationReport = validate(k, v)
            KeyValue(k, validationReport)
        }
        errorReports.to(appConfig.outgoingtopic)
        return validationStreams
    }

    fun validate( key: String, participant: JsonNode): String {
        LOG.info("Validating participant $key")
        try {
            val report = validator.validate(participant)
            return report
        } catch (e: Exception) {
            return "Invalid payload for participant $key"
        }
    }
}