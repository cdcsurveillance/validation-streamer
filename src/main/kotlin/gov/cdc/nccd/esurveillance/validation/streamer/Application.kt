package gov.cdc.nccd.esurveillance.validation.streamer

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("gov.cdc.nccd.esurveillance.validation.streamer")
                .mainClass(Application.javaClass)
                .start()
    }
}